FROM ubuntu:bionic as baseos
MAINTAINER danton <danton@prov47.tech>
# Update Base System
#
ENV  AZCLI_VERSION=2.0.45 \
     HUGO_VERSION=0.48 \
     GO_VERSION=1.11 \
     GOPATH=/projects/root/go \
     PATH=$PATH:/usr/local/go/bin:/root/go/bin \
     TERRAFORM_VERSION=0.11.8    
#
WORKDIR /tmp
#
RUN  sed -i -e 's/^deb-src/#deb-src/' /etc/apt/sources.list \
     && export DEBIAN_FRONTEND=noninteractive \
     && apt-get update \
     && apt-get upgrade -y --no-install-recommends \
     && apt-get install -y --no-install-recommends \
     apt-transport-https \
     bash \
     bash-completion \
     ca-certificates \
     curl \
     dnsutils \
     git-core \
     git \
     gnupg2 \
     htop \
     less \
     locales \
     lynx \
     mtr \
     nmap \
     openssh-client \
     p7zip-full \
     python \
     python-dev \
     python-pip \
     python-setuptools \
     tmux \
     vim \
     && locale-gen en_US.UTF-8 \
     && curl -L https://storage.googleapis.com/golang/go$GO_VERSION.linux-amd64.tar.gz | tar -C /usr/local -xz \
     && curl -L https://github.com/gohugoio/hugo/releases/download/v${HUGO_VERSION}/hugo_${HUGO_VERSION}_Linux-64bit.tar.gz | tar -C /usr/local/bin -xz \
     && curl https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip --output terraform.zip \
     && pip install --disable-pip-version-check --no-cache-dir azure-cli==${AZCLI_VERSION} shyaml \
     && 7z e terraform.zip \
     && cp terraform /usr/local/bin/terraform \
     && apt-get autoremove -y \
     && apt-get clean -y \
     && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* /usr/share/man /usr/share/doc   
#
WORKDIR /root
#
RUN git clone https://gitlab.com/prov47tech/dotfiles.git
#
RUN /root/dotfiles/install.sh
#
RUN rm -rf /root/dotfiles
#
CMD ["bash"]
